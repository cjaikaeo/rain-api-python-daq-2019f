from config import OPENAPI_AUTOGEN_DIR
import sys
sys.path.append(OPENAPI_AUTOGEN_DIR)

import pymysql as mysql
from config import DB_HOST, DB_USER, DB_PASSWD, DB_NAME
from openapi_server import models

db = mysql.connect(host=DB_HOST,user=DB_USER,passwd=DB_PASSWD,db=DB_NAME)


def get_basins():
    cs = db.cursor()
    cs.execute("SELECT basin_id,name FROM basin")
    result = [models.BasinShort(id,name) for id,name in cs.fetchall()]
    cs.close()
    return result


def get_basin_details(basin_id):
    cs = db.cursor()
    cs.execute("SELECT basin_id,name,area FROM basin WHERE basin_id=%s",[basin_id])
    basin_id,name,area = cs.fetchone()
    cs.close()
    return models.BasinFull(basin_id,name,area)


def get_stations(basin_id):
    cs = db.cursor()
    cs.execute("SELECT station_id,ename FROM station WHERE basin_id=%s",[basin_id])
    result = [models.StationShort(id,name) for id,name in cs.fetchall()]
    cs.close()
    return result


def get_station_details(station_id):
    return "Do something"
